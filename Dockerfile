FROM ubuntu:latest

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update
RUN apt-get -y install npm

COPY . .

RUN npm i
RUN npm run build

CMD ["node", "dist/main"]