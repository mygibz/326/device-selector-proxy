import { Controller, Get, Header, Param, Res } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(@Res() res) {
    // return "Welcome to this API, and now... GET OUT!!"
    return res.redirect("https://sgartschool.gitlab.io/gibz/m326/device-selector/")
  }
  
  @Get('laptop/:id')
  @Header('content-type', 'application/json')
  async getLaptop(@Param('id') id: number) {
    if (isNaN(id))
      return JSON.stringify({message: "the id has to be a number"})
      
    return await this.appService.getLaptopById(id);
  }

  @Get('search/:query')
  @Header('content-type', 'application/json')
  async getSearch(@Param('query') query: string) {
    return await this.appService.getSearchResults(query);
  }

  @Get('recommendations')
  @Header('content-type', 'application/json')
  getRecommendations(): string {
    return this.appService.getRecommendations();
  }
}
