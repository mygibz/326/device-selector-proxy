import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService, deleteFromCache } from './app.service';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService]
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('/laptop', () => {

    it('correct response structure', async () => {
      const response = JSON.parse(await appController.getLaptop(5179))
      expect(response).toHaveProperty('processor_core_count')
      expect(response).toHaveProperty('processor_frequency_mhz')
      expect(response).toHaveProperty('memory_mb')
      expect(response).toHaveProperty('storage_capacity_mb')
      expect(response).toHaveProperty('storage_type')
      expect(response).toHaveProperty('display_size_inch')
      expect(response).toHaveProperty('touchscreen')
      expect(response).toHaveProperty('pen')
      expect(response).toHaveProperty('usb_c')
      expect(response).toHaveProperty('thumbnail_url')
      expect(response).toHaveProperty('noteb_name')
      expect(response).toHaveProperty('min_price')
      expect(response).toHaveProperty('max_price')
    });

  });

  describe('/recommendations', () => {

    it('correct response format', () => {
      const response = JSON.parse(appController.getRecommendations())
      expect(response.constructor.name).toBe('Array')
    })

    it('cache works', async () => {
      // delete, if exist
      await deleteFromCache(5179)

      // check if delete worked
      let response = JSON.parse(appController.getRecommendations())
      expect(response.length).toBe(0)

      // "add laptop to cache"
      await appController.getLaptop(5179)

      // check if length now 1
      response = JSON.parse(appController.getRecommendations())
      expect(response.length).toBe(1)
    })

  })
});