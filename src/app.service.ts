import { firstValueFrom } from 'rxjs';
import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';

let cache = require('persistent-cache')()

const converter = source => { return {
  processor_core_count:       source?.cpu.cores,
  processor_frequency_mhz:    source?.cpu.base_speed * 1000,
  memory_mb:                  source?.memory.size * 1024,
  storage_capacity_mb:        (source?.primary_storage.cap + source?.secondary_storage.cap) * 100,
  storage_type:               source?.primary_storage.model.includes("SSD") ? "ssd" : "hdd", // TODO: Fix
  display_size_inch:          source?.display.size,
  touchscreen:                source?.display.touch === "yes",
  pen:                        source?.chassis.other_info.includes("Stylus"),
  usb_c:                      source?.chassis.peripheral_interfaces.includes("USB-C"),
  thumbnail_url:              source?.model_resources.thumbnail,
  noteb_name:                 source?.model_info['0'].noteb_name,
  min_price:                  source?.config_price_min,
  max_price:                  source?.config_price_max
}}

export const deleteFromCache = (id: number) => new Promise(resolve => { cache.delete(id, resolve) })

@Injectable()
export class AppService {
  // constructor(private httpService: HttpService) {}

  async getLaptopById(id: number) {
    // Grab from cache
    const cachedData = cache.getSync(id)

    if (cachedData == undefined) {
      const response = await this.proxy(`method=get_model_info&param[model_id]=${id}`)
      const finalData = converter(response.data.result[0]);

      // Store in cache
      // cache.put(id, finalData, () => console.log(`Cached data for ${id}`))
      cache.put(id, finalData, () => {})
      
      return JSON.stringify(finalData)
    } else {
      return JSON.stringify(cachedData)
    }
  }

  async getSearchResults(name: string) {
    const response = await this.proxy(`method=list_models&param[model_name]=${name}`)
    return JSON.stringify(response.data)
  }

  async proxy(query: string): Promise<any> {
    const httpService = new HttpService
    return await firstValueFrom(
      httpService.post(`https://noteb.com/api/webservice.php`, `apikey=q85KQhgq5EFEukG47B&${query}`)
    )
  }

  getRecommendations() {
    return JSON.stringify(
      cache.keysSync().map(k => { return {
        id: k,
        ...cache.getSync(k)
      }})
    )
  }

}
